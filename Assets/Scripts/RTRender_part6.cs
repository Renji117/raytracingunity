﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class RTRender_part6 : MonoBehaviour
{
    private Texture2D renderTexture;
    private Camera RTcamera;

    //weather or not to render in real time
    private Boolean RealTime = false;

    //How much of our screen resolution we render at
    private float RenderResolution = 1;

    //We create a new variable to hold all lights
    private Light[] Lights;

    private LayerMask collisionMask = 1 << 31;


    //Create render texture with screen size with resolution
    private void Awake()
    {
        renderTexture = new Texture2D(Convert.ToInt32(Screen.width * RenderResolution), Convert.ToInt32(Screen.height*RenderResolution));
        RTcamera = GetComponent<Camera>();
    }

    //In Start we only render if we are not real time
    void Start()
    {
        GenerateColliders();
        if (!RealTime)
        {
            RayTrace();
        }

    }

    //In the new Update, we only render if we are real time
    void Update()
    {
        if (RealTime)
        {
            RayTrace();
        }
    }


    private void GenerateColliders()
    {
        //Loop through all mesh filters
        foreach (MeshFilter mf in FindObjectsOfType(typeof(MeshFilter)) as MeshFilter[])
        {

            //Only if they have a MeshRenderer attached
            //They might not... who knows?
            if (!mf.GetComponent<MeshCollider>())
            {
                //Create a new object we will use for rendering
                MeshCollider mesh = mf.gameObject.AddComponent<MeshCollider>();
            }
        }
    }


    //Trace a single point for all lights
    private Color TraceLight(Vector3 pos, Vector3 normal)
    {
        //We set the starting light to that of the render settings ambient light
        //This makes it easier to predict how it will look when we render it
        Color returnColor = RenderSettings.ambientLight;

        //We loop through all the lights and perform a light addition with each
        foreach(Light light in Lights)
        {
            if(light.enabled)
            {
                //Add the light that this light source casts to the color of this point
                returnColor += LightTrace(light, pos, normal);
            }
        }
        //return the color of this point according to lighting
        return returnColor;
    }

    //Trace a single point for a single light
    private Color LightTrace(Light light, Vector3 pos, Vector3 normal)
    {
        float dot;
        //Only trace if it's a directional light
        if (light.type == LightType.Directional)
        {
            //calculate the dot product
            dot = Vector3.Dot(-light.transform.forward, normal);

            //only perform lighting calculations, if the dot is more than 0
            if (dot > 0)
            {
                /*
                This needs some explaining:
                All we do here, is cast a ray indefinately in the opposite direction
                Of the way the directional light is facing. If this ray hits an object, it means
                that no light is recieved from this light source at this point,
                so we return black.
                If this ray does not hit, it means this point is recieving light from this light source
                so we return the color of the light, multiplied by it's intensity
                */
                if (Physics.Raycast(pos, -light.transform.forward, Mathf.Infinity))
                {
                    return Color.black;
                }
                return light.color * light.intensity * dot;
            }
        }
        else
        {
            //calculate the distance between our point and the light
            float distance = Vector3.Distance(pos, light.transform.position);

            //Lets calculate the direction from our point, to the light.
            //we will need this for both the normal shading and also shadow checking
            Vector3 direction = (light.transform.position - pos).normalized;

            //Now we calculate the dot product of the direction and the normal.
            dot = Vector3.Dot(normal, direction);


            //No matter if the light is point or spot, it still has a range and we account for that
            if (distance < light.range && dot > 0)
            {
                //Now we have to check for which type of light it is
                if (light.type == LightType.Point)
                {
                    //Raycast as we described
                    if (Physics.Raycast(pos, direction, distance))
                    {
                        return Color.black;
                    }
                    //Amd then our new formula
                    return light.color * light.intensity * dot * (1 - light.range / distance);
                }
                else if (light.type == LightType.Spot)
                {
                    float dot2 = Vector3.Dot(-light.transform.forward, normal);

                    if (dot2 < (1 - light.spotAngle / 180))
                    {
                        if (Physics.Raycast(pos, direction, distance))
                        {
                            return Color.black;
                        }

                        return light.color * light.intensity * dot * (1 - light.range / distance) *
                               ((dot2 / (1 - light.spotAngle / 180)));
                    }
                }
            }
        }
        //we are outside of the lights range, so no need for light
        return Color.black;
    }

    //The function that renders the entire scene to a texture
    private void RayTrace()
    {
        Lights = FindObjectsOfType(typeof(Light)) as Light[];

        for (var x = 0; x < renderTexture.width; x++)
        {
            for (var y = 0; y < renderTexture.height; y++)
            {
                //Now that we have an x/y value for each pixel, we need to make that into a 3d ray according to the camera we are attached to
                Ray ray = RTcamera.ScreenPointToRay(new Vector3(x / RenderResolution, y / RenderResolution, 0));

                //Now lets call a function with this ray and apply it's return value to the pixel we are on we will define this function afterwards
                renderTexture.SetPixel(x, y, TraceRay(ray));
            }
        }
        //We also need to apply the changes we have made to the texture
        //This is a part that can cause much pain and frustraction if forgotten
        //So don't forget ;)
        renderTexture.Apply();
    }

    private Color TraceRay(Ray ray)
    {
         //We fist have to create the hit variable
        RaycastHit Hit;

        //The color this function will return
        Color returnColor = Color.black;

        //Now we parse it in as another argument
        if (Physics.Raycast(ray, out Hit))
        {
            //now we can get all kinds of information out of the "hit"
            //like hit.distance, hit.point, all of which will be usefull later on

            Material mat;
            //Get the material attached to the renderer of the collider we hit
            //if we used hit.transform instead, we would encounter bugs with rigidbodys
            //so we use collider
            mat = Hit.collider.GetComponent<Renderer>().material;

            //Instead of returning or settings the color, we simply add the color
            if (mat.mainTexture)
            {
                returnColor += ((Texture2D)mat.mainTexture).GetPixelBilinear(Hit.textureCoord.x, Hit.textureCoord.y);
            }
            else
            {
                returnColor += mat.color;
            }

            returnColor *= TraceLight(Hit.point + Hit.normal * 0.0001f, Hit.normal);
        }
        return returnColor;
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), renderTexture);
    }






}

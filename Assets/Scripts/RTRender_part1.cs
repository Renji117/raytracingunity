﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;


public class RTRender_part1 : MonoBehaviour
{
    private Texture2D renderTexture;
    private Camera RTcamera;

    //weather or not to render in real time
    private Boolean RealTime = false;

    //How much of our screen resolution we render at
    private float RenderResolution = 1;


    //Create render texture with screen size with resolution
    private void Awake()
    {
        renderTexture = new Texture2D(Convert.ToInt32(Screen.width * RenderResolution), Convert.ToInt32(Screen.height*RenderResolution));
        RTcamera = GetComponent<Camera>();
    }

    //The function that renders the entire scene to a texture
    private void RayTrace()
    {
        for (var x = 0; x < renderTexture.width; x++)
        {
            for (var y = 0; y < renderTexture.height; y++)
            {
                //Now that we have an x/y value for each pixel, we need to make that into a 3d ray according to the camera we are attached to
                Ray ray = RTcamera.ScreenPointToRay(new Vector3(x / RenderResolution, y / RenderResolution, 0));

                //Now lets call a function with this ray and apply it's return value to the pixel we are on we will define this function afterwards
                renderTexture.SetPixel(x, y, TraceRay(ray));
            }
        }
        //We also need to apply the changes we have made to the texture
        //This is a part that can cause much pain and frustraction if forgotten
        //So don't forget ;)
        renderTexture.Apply();
    }

    private static Color TraceRay(Ray ray)
    {
        if (Physics.Raycast(ray))
        {
            return Color.white;
        }
        else return Color.black;
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), renderTexture);
    }

    //In Start we only render if we are not real time
    void Start()
    {
        if(!RealTime)
        {
            RayTrace();
        }
       
    }

    //In the new Update, we only render if we are real time
    void Update()
    {
        if (RealTime)
        {
            RayTrace();
        }
    }




}
